FROM alpine:latest

RUN apk add --update git lua-dev gcc make openssl-dev pcre-dev g++ python2

WORKDIR /root

RUN git clone https://github.com/lefcha/imapfilter.git

WORKDIR /root/imapfilter

RUN make all && make install

WORKDIR /root

RUN git clone https://github.com/google/gmail-oauth2-tools.git \
  && cp gmail-oauth2-tools/python/oauth2.py /usr/local/bin/ \
  && chmod +x /usr/local/bin/oauth2.py

RUN mkdir /root/.imapfilter

WORKDIR /root

RUN rm -fr imapfilter gmail-oauth2-tools

# CMD imapfilter


ADD imapfilter.sh /imapfilter.sh
RUN chmod +x /imapfilter.sh

ENTRYPOINT ["/imapfilter.sh"]
# CMD /imapfilter.sh
